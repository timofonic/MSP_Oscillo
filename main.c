/* Exercice 1
* D�riv� de l'exemple 001 de FreeRTOS
* L'affichage d'infos est remplac� par le changement d'�tat d'une LED
*/

#include "main.h"

/* Standard demo includes. */
//#include "ParTest.h"
//#include "dynamic.h"
//#include "comtest2.h"
//#include "GenQTest.h"
//#include "TimerDemo.h"
//#include "countsem.h"

/*
 * Configures clocks, LCD, port pints, etc. necessary to execute this demo.
 */
static void prvSetupHardware( void );

void main( void )
{
	/* Configure the peripherals used by this demo application.  This includes
	configuring the joystick input select button to generate interrupts. */
	prvSetupHardware();
	InitButton();
	InitLCD();
	InitADC();

	/* Create the other task in exactly the same way. */
	xTaskCreate( ReadButton, "Button", 100, NULL, 1, NULL );
	xTaskCreate( ReadADC, "ADC", 100, NULL, 1, NULL );

	xTaskCreate( DisplaySignal, "Signal", 100, NULL, 2, NULL );

	xTaskCreate( DisplayMenu, "Menu", 100, NULL, 3, NULL );
	xTaskCreate( DisplayInfo, "Info", 100, NULL, 3, NULL );

	/* Start the scheduler. */
	vTaskStartScheduler();

	for( ;; );
}

/*-----------------------------------------------------------*/
static void prvSetupHardware( void )
{
	taskDISABLE_INTERRUPTS();
	
	/* Disable the watchdog. */
	WDTCTL = WDTPW + WDTHOLD;
  
	//halBoardInit();
	Board_init();

	// Set Vcore to accomodate for max. allowed system speed
	SetVCore(3);

	// Use 32.768kHz XTAL as reference
	LFXT_Start(XT1DRIVE_0);

	// Set system clock to max (25MHz)
	Init_FLL_Settle(25000, 762);

	SFRIFG1 = 0;
	SFRIE1 |= OFIE;

	//LFXT_Start( XT1DRIVE_0 );
	//hal430SetSystemClock( configCPU_CLOCK_HZ, configLFXT_CLOCK_HZ );

	//halButtonsInit( BUTTON_ALL );
	Buttons_init(BUTTON_ALL);
	//halButtonsInterruptEnable( BUTTON_SELECT );
	Buttons_interruptEnable(BUTTON_S2);

	/* Initialise the LCD, but note that the backlight is not used as the
	library function uses timer A0 to modulate the backlight, and this file
	defines	vApplicationSetupTimerInterrupt() to also use timer A0 to generate
	the tick interrupt.  If the backlight is required, then change either the
	halLCD library or vApplicationSetupTimerInterrupt() to use a different
	timer.  Timer A1 is used for the run time stats time base6. */
	//halLcdInit();
	//halLcdSetContrast( 100 );
	//halLcdClearScreen();

	//halLcdPrintLine( " www.FreeRTOS.org", 0,  OVERWRITE_TEXT );
}

void vApplicationTickHook( void )
{
static unsigned long ulCounter = 0;

	/* Is it time to toggle the LED again? */
	ulCounter++;

	/* Just periodically toggle an LED to show that the tick interrupt is
	running.  Note that this access LED_PORT_OUT in a non-atomic way, so tasks
	that access the same port must do so from a critical section. */
	if( ( ulCounter & 0xff ) == 0 )
	{
		/*if( ( LED_PORT_OUT & LED_1 ) == 0 )
		{
			LED_PORT_OUT |= LED_1;
		}
		else
		{
			LED_PORT_OUT &= ~LED_1;
		}*/
		Board_ledToggle(LED1);
	}
}

#pragma vector=PORT2_VECTOR
__interrupt void prvSelectButtonInterrupt( void )
{

	P2IFG = 0;

	/* If writing to xLCDQueue caused a task to unblock, and the unblocked task
	has a priority equal to or above the task that this interrupt interrupted,
	then lHigherPriorityTaskWoken will have been set to pdTRUE internally within
	xQueuesendFromISR(), and portEND_SWITCHING_ISR() will ensure that this
	interrupt returns directly to the higher priority unblocked task. */
}

/* The MSP430X port uses this callback function to configure its tick interrupt.
This allows the application to choose the tick interrupt source.
configTICK_VECTOR must also be set in FreeRTOSConfig.h to the correct
interrupt vector for the chosen tick interrupt source.  This implementation of
vApplicationSetupTimerInterrupt() generates the tick from timer A0, so in this
case configTICK_VECTOR is set to TIMER0_A0_VECTOR. */
void vApplicationSetupTimerInterrupt( void )
{
const unsigned short usACLK_Frequency_Hz = 32768;

	/* Ensure the timer is stopped. */
	TA0CTL = 0;

	/* Run the timer from the ACLK. */
	TA0CTL = TASSEL_1;

	/* Clear everything to start with. */
	TA0CTL |= TACLR;

	/* Set the compare match value according to the tick rate we want. */
	TA0CCR0 = usACLK_Frequency_Hz / configTICK_RATE_HZ;

	/* Enable the interrupts. */
	TA0CCTL0 = CCIE;

	/* Start up clean. */
	TA0CTL |= TACLR;

	/* Up mode. */
	TA0CTL |= MC_1;
}

void vApplicationIdleHook( void )
{
	/* Called on each iteration of the idle task.  In this case the idle task
	just enters a low(ish) power mode. */
	__bis_SR_register( LPM1_bits + GIE );
}
/*-----------------------------------------------------------*/
