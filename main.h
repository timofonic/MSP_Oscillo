/*
 * Projet	: Oscilloscope
 * Auteurs	: Stephan Kern
 * 			: Tim Tuuva
 * fichier	: main.h
 * language	: ANSI C + specificite uC msp430
 *
 * date	cr�ation: 02.10.2015
 *
 * Description :
 * Fichier d'inclusion des varaibles globales, defines et des prototypes de fct
 *
 */
/* Standard includes. */
#include <stdio.h>

/* FreeRTOS includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "queue.h"

/* Hardware includes. */
#include "msp430.h"
#include "HAL_Dogs102x6.h"
#include "HAL_Buttons.h"
#include "HAL_Board.h"
#include "HAL_PMM.h"
#include "hal_UCS.h"
#include "SMC_hal.h"

// BUTTON
#define PressTime	10
#define S1			1
#define S2			2
#define S1_2		-1

// LCD
#define DOGS102x6_X_SIZE	102
#define DOGS102x6_Y_SIZE	64

// ADC
#define ADC_BUFFER_SIZE		DOGS102x6_X_SIZE

// FLAG
#define UPDATE		BIT0
#define TRIG_UP		BIT1

// ETAT MENU
typedef enum {
			MENU_GAIN = 0, 
			REG_GAIN,
			MENU_TIME,
			REG_TIME,
			MENU_TRIG,
			REG_FLANC,
			REG_THRES,
			MENU_ACQ} MENU;
/*
 * Variables globales
 */

/* MENU Variables d'Etat:
 * #MENU:     	Etat:
 * #MENU_ACQ   	7: ACQUISITION active
 * #REG_THRES 	6: Reglage THRES
 * #REG_FLANC 	5: Reglage FLANC
 * #MENU_TRIG 	4: Menu TRIG
 * #REG_TIME   	3: Reglage TIME
 * #MENU_TIME  	2: Menu TIME
 * #REG_GAIN   	1: Reglage GAIN
 * #MENU_GAIN  	0: Menu Gain
 */

/* FLAGS est un registre 4 bits contenant:
 * #BOOL:	|    BIT1
 * #TRIGGER	| UP:1/DOWN:0
 *
 * #BOOL:      |    BIT0
 * #UPDATE LCD | YES:1/NO:0
 */
extern volatile MENU ETAT_MENU;
extern volatile char FLAGS;
extern volatile uint16_t ADC_Threshold;
extern volatile uint16_t OSC_Gain;
extern volatile uint16_t OSC_Time;
extern volatile uint16_t ADC_Buffer[DOGS102x6_X_SIZE];

/*
 * Prototypes des fonctions
 */
void InitButton(void);
void InitLCD(void);
void InitADC(void);

void HandleButton(unsigned int button,unsigned int counter);

/* 
 * Prototypes des Taches
 */
void ReadButton(void);
void DisplayMenu(void);
void DisplaySignal(void);
void DisplayInfo(void);
void ReadADC(void);
