#include "main.h"

void InitButton(void)
{
	CLR_SW1_DIR();
	CLR_SW2_DIR();

	SET_SW1_OUT();
	SET_SW2_OUT();

	CLR_SW1_SEL();
	CLR_SW2_SEL();

	SET_SW1_REN();
	SET_SW2_REN();
}

void ReadButton(void)
{
 	TickType_t xLastWakeTime;
	xLastWakeTime = xTaskGetTickCount();

	static char FlagButtonDown = 0;
	static unsigned int counter = 0;
	static int button = 0;

	enum BOUTON {S2_S1_ON=0, S2_S1_COUNT,
		  		 S2_ON, S2_COUNT,
		  		 S1_ON, S1_COUNT,
		  		 SX_OFF, SX_RELEASED};

	for( ;; )
	{
		enum BOUTON state = (P2IN & BIT2) | ((P1IN & BIT7) >> 6)
										  | FlagButtonDown;

		/* Machine d'etat a trois variables modulable
		S2 | S1 | Flag
		##############
		0    0    0  # 0: S2_S1_ON: 	Les deux boutons simultanement
		0    0    1  # 1: S2_S1_COUNT: Comptage simultanee
		0    1    0  # 2: S2_ON:		Bouton 2 Presse
		0    1    1  # 3: S2_COUNT:	Comptage S2
		1    0    0  # 4: S1_ON:		Bouton 1 Presse
		1    0    1  # 5: S1_COUNT:	Comptage S1
		1    1    0  # 6: SX_OFF:		Etat habituel pas d'Input
		1    1    1  # 7: SX_RELEASED:	Etat de relache de 'n' bouton
		*/

		switch(state)
		{
			case S2_S1_ON:
				// Button 1 & Bouton 2 presse
				FlagButtonDown = 1;
				// Assignation bouton presse
				button = S1_2;
				// Initialisation compteur
				counter = 0;
				break;

			case S2_S1_COUNT:
				// Comptage temps de pression S1 & S2
				counter++;
				break;

			case S2_ON:
				// Button 2 presse
				FlagButtonDown = 1;
				// Assignation bouton presse
				button = S2;
				// Initialisation compteur
				counter = 0;
				break;

			case S2_COUNT:
				// Comptage temps de pression S2
				counter++;
				break;

			case S1_ON:
				// Button 1 presse
				FlagButtonDown = 1;
				// Assignation bouton presse
				button = S1;
				// Initialisation compteur
				counter = 0;
				break;

			case S1_COUNT:
				// compatage temps de pression S1
				counter++;
				break;

			case SX_OFF:
				// Aucune pression
				break;

			case SX_RELEASED:
				// Fonction qui effectue un changement
				// en fonction du bouton et de son temps de pression
				HandleButton(button,counter);
				button = 0;
				FlagButtonDown = 0;
				break;
		}
	    vTaskDelayUntil(&xLastWakeTime, 25);
	}
}

void HandleButton(unsigned int button, unsigned int counter)
{
	// On demande a mettre a jour le LCD
	FLAGS |= UPDATE;
	switch(button)
	{
		case S1:
			switch(ETAT_MENU)
			{
				case MENU_GAIN:
				case REG_GAIN:
					ETAT_MENU = MENU_TIME;
					break;

				case MENU_TIME:
				case REG_TIME:
					ETAT_MENU = MENU_TRIG;
					break;

				case MENU_TRIG:
					ETAT_MENU = MENU_ACQ;
					//TODO LIGNE REPETITIVE !!!!
					ADC12CTL0 &= ~(ADC12ENC | ADC12SC);
					ADC12MCTL0 = ADC12SREF_0 | ADC12INCH_6 | ADC12EOS;
					ADC12CTL0 |= ADC12ENC | ADC12SC;
					break;

				case REG_FLANC:
					ETAT_MENU = REG_THRES;
					break;
				case REG_THRES:
					ETAT_MENU = MENU_ACQ;
					//TODO LIGNE REPETITIVE !!!!
					ADC12CTL0 &= ~(ADC12ENC | ADC12SC);
					ADC12MCTL0 = ADC12SREF_0 | ADC12INCH_6 | ADC12EOS;
					ADC12CTL0 |= ADC12ENC | ADC12SC;
					break;

				case MENU_ACQ:
					ETAT_MENU = MENU_GAIN;
					//TODO LIGNE REPETITIVE !!!!
					ADC12CTL0 &= ~(ADC12ENC | ADC12SC);
					ADC12MCTL0 = ADC12SREF_0 | ADC12INCH_5 | ADC12EOS;
					ADC12CTL0 |= ADC12ENC | ADC12SC;
					break;
			}
			break;// Fin Case S1

		case S2:
			switch(ETAT_MENU)
			{
				case MENU_GAIN:
					ETAT_MENU = REG_GAIN;
					break;
				case MENU_TIME:
					ETAT_MENU = REG_TIME;
					break;
				case MENU_TRIG:
					ETAT_MENU = REG_FLANC;
					break;

				default:
					break;
			}
			break;// Fin Case S2

		case S1_2:
			//Nada
			break;
	}
}
