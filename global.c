#include "main.h"
/*
 * Variables globales
 */

/* MENU Variables d'Etat:
 * #MENU:     	Etat:
 * #ACQ	    	7: ACQUISITION active
 * #REG_THRES 	6: Reglage THRES
 * #REG_FLANC 	5: Reglage FLANC
 * #MENU_TRIG 	4: Menu TRIG
 * #REG_TIME   	3: Reglage TIME
 * #MENU_TIME  	2: Menu TIME
 * #REG_GAIN   	1: Reglage GAIN
 * #MENU_GAIN  	0: Menu Gain
 */

/* FLAGS est un registre 4 bits contenant:
 * #BOOL:	|    BIT1
 * #TRIGGER	| UP:1/DOWN:0
 *
 * #BOOL:      |    BIT0
 * #UPDATE LCD | YES:1/NO:0
 */

volatile MENU ETAT_MENU = 0;
volatile char FLAGS = 0;
volatile uint16_t ADC_Threshold = 0;
volatile uint16_t OSC_Gain = 0;
volatile uint16_t OSC_Time = 0;

// Tableaux de mesure
volatile uint16_t ADC_Buffer[DOGS102x6_X_SIZE] = {0};
