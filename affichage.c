#include "main.h"

void InitLCD(void)
{
	Dogs102x6_init();
	Dogs102x6_backlightInit();
	Dogs102x6_setBacklight(1);
	Dogs102x6_setContrast(5);
	Dogs102x6_refresh(1);

	//Mode Acquisition au depart:
	ETAT_MENU = MENU_ACQ;
	//Demande d'update le LCD
	FLAGS = 1;
}

void DisplayMenu(void)
{
 	TickType_t xLastWakeTime;
	xLastWakeTime = xTaskGetTickCount();

	char str[10];
	static unsigned int threshold = 0;

	for( ;; )
	{
		/*if(!(FLAGS & UPDATE))
		{
			vTaskDelayUntil(&xLastWakeTime, 200);
			continue;
		}*/

		/*
		 * Affichage du menu superieur
		 */
		//Fancy Menu Decoration
		Dogs102x6_verticalLineDraw(0, 7, 0, (ETAT_MENU == MENU_ACQ));
		Dogs102x6_verticalLineDraw(0, 7, 25,1);
		Dogs102x6_verticalLineDraw(0, 7, 50,(ETAT_MENU == MENU_TRIG) 
										 || (ETAT_MENU == REG_FLANC)
										 || (ETAT_MENU == REG_THRES));

		Dogs102x6_verticalLineDraw(0, 7, 51,1);
		Dogs102x6_verticalLineDraw(0, 7, 76,1);
		Dogs102x6_verticalLineDraw(0, 7, 77,(ETAT_MENU == MENU_GAIN)
										 || (ETAT_MENU == REG_GAIN));

		Dogs102x6_horizontalLineDraw(0, DOGS102x6_X_SIZE, DOGS102x6_Y_SIZE-9,0);

		//Menu Onglets
		Dogs102x6_stringDraw(0, 1,"MAIN", (ETAT_MENU != MENU_ACQ));

		Dogs102x6_stringDraw(0,26,"TRIG", (ETAT_MENU != MENU_TRIG) 
									   && (ETAT_MENU != REG_FLANC)
									   && (ETAT_MENU != REG_THRES));

		Dogs102x6_stringDraw(0,52,"TIME", (ETAT_MENU != MENU_TIME) 
									   && (ETAT_MENU != REG_TIME));

		Dogs102x6_stringDraw(0,78,"GAIN", (ETAT_MENU != MENU_GAIN)
									   && (ETAT_MENU != REG_GAIN));

		switch(ETAT_MENU)
		{
			case MENU_GAIN:
			case REG_GAIN:
				snprintf(str, 10, "%4d", OSC_Gain);

				Dogs102x6_stringDraw(DOGS102x6_Y_SIZE,0," Gain:           ", (ETAT_MENU == REG_GAIN));
				Dogs102x6_stringDraw(DOGS102x6_Y_SIZE,50,str, (ETAT_MENU == REG_GAIN));
				//Dogs102x6_stringDraw(DOGS102x6_Y_SIZE,12," V    ", (ETAT_MENU == REG_GAIN));
				break;

			case MENU_TIME:
			case REG_TIME:
				snprintf(str, 10, "%4d", OSC_Time);

				Dogs102x6_stringDraw(DOGS102x6_Y_SIZE,0," Time:           ", (ETAT_MENU == REG_TIME));
				Dogs102x6_stringDraw(DOGS102x6_Y_SIZE,50,str, (ETAT_MENU == REG_TIME));
				break;

			case MENU_TRIG:
			case REG_FLANC:
			case REG_THRES:
				//Info
				if(FLAGS & TRIG_UP)
					Dogs102x6_stringDraw(DOGS102x6_Y_SIZE,0," Flanc:/ ", (ETAT_MENU == REG_FLANC));

				else
					Dogs102x6_stringDraw(DOGS102x6_Y_SIZE,0," Flanc:\\ ", (ETAT_MENU == REG_FLANC));

				Dogs102x6_stringDraw(DOGS102x6_Y_SIZE,DOGS102x6_X_SIZE*0.5-1," Niveau  ", (ETAT_MENU == REG_THRES));

				//Ligne Trigger
				Dogs102x6_horizontalLineDraw(1, DOGS102x6_X_SIZE, threshold,1);
				threshold = (DOGS102x6_Y_SIZE - (ADC_Threshold/86) - 9);
				Dogs102x6_horizontalLineDraw(1, DOGS102x6_X_SIZE, threshold,0);

				break;

			default:
				break;
		}
		// On ne demande pas a rafraichir tant que rien ne change
		FLAGS &= ~UPDATE;

	    vTaskDelayUntil(&xLastWakeTime, 100);
	}
}

void DisplayInfo(void)
{
 	TickType_t xLastWakeTime;
	xLastWakeTime = xTaskGetTickCount();

	for( ;; )
	{
		// On ne fait rien si on est pas en mode oscilloscope
		if(ETAT_MENU == MENU_ACQ || ETAT_MENU == REG_THRES)
		{
			//Temps
			Dogs102x6_horizontalLineDraw(0, DOGS102x6_X_SIZE,8,0);

			Dogs102x6_verticalLineDraw(9, 9+2, 25,0);
			Dogs102x6_verticalLineDraw(9, 9+2, 51,0);
			Dogs102x6_verticalLineDraw(9, 9+2, 76,0);
			Dogs102x6_verticalLineDraw(9, 9+2, 102,0);

			//Amplitude
			Dogs102x6_verticalLineDraw(8, DOGS102x6_Y_SIZE-9, 0,0);
			Dogs102x6_horizontalLineDraw(0, 2, 8,0);
			Dogs102x6_horizontalLineDraw(0, 2, 16,0);
			Dogs102x6_horizontalLineDraw(0, 2, 24,0);
			Dogs102x6_horizontalLineDraw(0, 2, 32,0);
			Dogs102x6_horizontalLineDraw(0, 2, 40,0);
			Dogs102x6_horizontalLineDraw(0, 2, 48,0);

			//Info
			Dogs102x6_stringDraw(DOGS102x6_Y_SIZE,0," F:/|A: XX|T: X  ", 1);
		}
	    vTaskDelayUntil(&xLastWakeTime, 100);
	}
}

void DisplaySignal(void)
{
 	TickType_t xLastWakeTime;
	xLastWakeTime = xTaskGetTickCount();

	static unsigned int y[DOGS102x6_X_SIZE] = {0};
	static unsigned int x = 0;

	for( ;; )
	{
		// On ne fait rien si on est pas en mode oscilloscope
		if(ETAT_MENU != MENU_ACQ)
		{
			vTaskDelayUntil(&xLastWakeTime, 5);
			continue;
		}

		// On stocke les anciennes valeurs de l'ADC encore affiche sur le LCD

		// On reboucle le tableau si on arrive au bout du LCD
		if(x == DOGS102x6_X_SIZE)
			x = 0; 

		/*
		 * On efface l'ancien pixel en (x,y)
		 * On assigne les nouvelles valeurs fournit par ADC_Buffer
		 * On affiche le nouveau pixel
	   	 */
		Dogs102x6_pixelDraw(x,y[x], 1);
		y[x] = DOGS102x6_Y_SIZE - (ADC_Buffer[x]/86) - 9;
		Dogs102x6_pixelDraw(x,y[x], 0);

		x++;
	    vTaskDelayUntil(&xLastWakeTime, 5);
	}
}
