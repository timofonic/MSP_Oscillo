#include "main.h"

void InitADC(void)
{
	/*
     * On active l'entree:
	 * PIN 6.6
	 * PIN 8.8: WHEEL
	 */
	P6SEL = 0x0F;
	P6DIR |= BIT6;
	P6OUT |= BIT6;

    //P6DIR &= ~BIT5;
    P8OUT |= BIT0;                 // POT_PWR
    P8DIR |= BIT0;

	/*
     * On configure l'ADC:
	 * ADC12SHT0_2: t_sample = 2^4 = 16 ADC12CLK
	 * ADC12ON: Activation ADC12
	 * ADC12MSC: Declenchement suivant automatique
	 * ADC12CONSEQ_2: Conversion repetitive
	 */
    ADC12CTL0 = ADC12SHT0_2 | ADC12ON | ADC12MSC;
    ADC12CTL1 = ADC12CSTARTADD_0 | ADC12SHP | ADC12DIV_0 | ADC12SSEL_1 | ADC12CONSEQ_2;

    /* 
	 * ADC12SREF_0: GND REF
	 * ADC12INCH_6: Input sur le PIN 6
	 * ADC12INCH_5: Input sur Wheel
	 */
	ADC12MCTL0 = ADC12SREF_0 | ADC12INCH_6 | ADC12EOS;

	// Autorisation de l'ADC et mise en route
	ADC12CTL0 |= ADC12ENC | ADC12SC;
}

void ReadADC(void)
{
 	TickType_t xLastWakeTime;
	xLastWakeTime = xTaskGetTickCount();

	P6DIR |= BIT6;
	P6OUT |= BIT6;

	int slope = 0;
	static char index = 0;

	for( ;; )
	{
		// Si on a parcouru tout le buffer du ADC on boucle au debut
		if(index >= ADC_BUFFER_SIZE)
		{
			/*
			 * On calcule la pente de la courbe en utilisant:
			 * la mesure courante: ADC12MEM0
			 * la moyenne des deux dernieres mesures
	         */
			slope = ADC12MEM0
					- (ADC_Buffer[ADC_BUFFER_SIZE-1]
					+  ADC_Buffer[ADC_BUFFER_SIZE-2])/2;

			/*
			 * On effectue le bouclage du buffer seulement si:
			 * la valeur mesure est superieur au niveau d'activation du trigger
			 * que la pente soit NEGATIVE FLAGS = xxxx xx0x
			 * que la pente soit POSITIVE FLAGS = xxxx xx1x
			 */
			//if(ADC12MEM0 >= ADC_Threshold && ((~FLAGS & TRIG_UP) && (slope <= 0) || (FLAGS & TRIG_UP) && (slope > 0)))
			index = 0;
		}
		else
		{
			switch(ETAT_MENU)
			{
				case MENU_ACQ:
					ADC_Buffer[index++] = ADC12MEM0;
					continue;

				case REG_GAIN:
					OSC_Gain = ADC12MEM0;
					break;

				case REG_TIME:
					OSC_Time = ADC12MEM0;
					break;

				case REG_FLANC:
					if(ADC12MEM0 > 2048)
						FLAGS |= TRIG_UP;
					else
						FLAGS &= ~TRIG_UP;
					break;

				case REG_THRES:
					ADC_Threshold = ADC12MEM0;
					break;

				default:
					break;
			}
		}
		vTaskDelayUntil(&xLastWakeTime, 1);
	}
}
